/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.peerinstruction_contextodepersistencia;
/**
 *
 * @author kaique
 */
public class Main {
    public static void main(String[] args) {
        CRUD crud = new CRUD();
        
        // Cadastrar
        crud.cadastrar(new Pessoa("Kaique Candido", 21));
        
        // Pesquisar
        // Verificar o id persistido na tabela Pessoa
        int idPessoa = 0;
        Pessoa p = crud.pesquisar(idPessoa);
        System.out.println(p);
        
        // Atualizar
        Pessoa pessoa = crud.pesquisar(idPessoa);
        pessoa.setNome("Kaique Candido de Oliveria");        
        crud.atualizar(pessoa);
        
        // Remover
        crud.remover(crud.pesquisar(idPessoa));
        
    }
}
