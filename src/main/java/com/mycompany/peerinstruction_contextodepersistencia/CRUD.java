/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.peerinstruction_contextodepersistencia;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author kaique
 */
public class CRUD {

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistence");
    EntityManager em = factory.createEntityManager();

    public void cadastrar(Pessoa p) {
        em.getTransaction().begin();
        em.persist(p);
        em.getTransaction().commit();
        em.close();
    }

    public void remover(Pessoa p) {
        em.getTransaction().begin();
        em.remove(p);
        em.getTransaction().commit();
        em.close();
    }

    public void atualizar(Pessoa p) {
        em.getTransaction().begin();
        em.merge(p);
        em.close();
    }

    public Pessoa pesquisar(int idPessoa) {
        return em.find(Pessoa.class, idPessoa);
    }
}
